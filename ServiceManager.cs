﻿using CommonLibrary.Helpers;
using ParserClient.Services;
using NLog;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Configuration;
using ParserClient.Crawler;

namespace ParserClient
{
    class ServiceManager
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();

        private CancellationTokenSource _cancel = null;

        public ServiceManager()
        {
            _cancel = new CancellationTokenSource();
        }


        private void PrintHelpMessage()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Сервис проверки страниц источников на наличи новых статей.");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Сервис проверки уже известных сайтов.");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("Сервис проверки еще не индексированных сайтов.");
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("==================================");
            Console.WriteLine("\tДля запуска: start.");
            Console.WriteLine("\tДля остановки: stop.");
            Console.WriteLine("==================================");
        }

        private void PrintServiceInfo(string serviceName, System.Timers.Timer timer)
        {
            Console.WriteLine("{0}", serviceName);
            Console.WriteLine("\t Автообновление: {0}", timer.AutoReset);
            Console.WriteLine("\t Интервал запуска: {0}s", timer.Interval / 1000);
            Console.WriteLine("\t Включен: {0}", timer.Enabled);
            Console.WriteLine();

            Console.ResetColor();
        }

        public void Start()
        {
            /* 
                Возможно надо будет добавить другие задачи, поэтому каждая описывается отдельно.
            */

            CrawlerArgs crawlerParametrs = CrawlerPramsFromConfig();

            // Проверка уже исвестных сайтов, для поиска новых списков статей
            System.Timers.Timer timerExploreKnownDomains = TimerFor("ExploreKnownDomains");
            timerExploreKnownDomains.Elapsed += async (o, e) => {

                IRemoteCoreServiceProvider service = new RemoteCoreService();


                var exploreKnownDomains = new ExploreWebSite(service, crawlerParametrs, ConsoleColor.Blue);

                exploreKnownDomains.CancellationToken = _cancel.Token;
                exploreKnownDomains.Logger = logger;
                try
                {
                    Uri host = await service.NextExistingDomain();

                    if (host != null)
                        await exploreKnownDomains.Process(host, await service.KnownPagesForDomain(host.Host));
                    else
                        Console.WriteLine("Нет известных доменов для проверки");
                }catch(OperationCanceledException ex)
                {
                    Console.WriteLine("Операция отменена");
                }
                catch (ServerException ex)
                {
                    HandleServerException(ex);
                }
                catch (Exception ex)
                {
                    HandleOtherException(ex);
                }
                //exploreKnownDomains.RunTask(task, _cancel);

            };


            // Проверка новых сайтов, для поиска страниц со списками статей
            System.Timers.Timer timerExploreNewDomains = TimerFor("ExploreNewDomains");
            timerExploreNewDomains.Elapsed += async (o, e) =>
            {
                IRemoteCoreServiceProvider service = new RemoteCoreService();
                
                var exploreNewDomains = new ExploreWebSite(service, crawlerParametrs, ConsoleColor.Magenta);

                exploreNewDomains.CancellationToken = _cancel.Token;
                exploreNewDomains.Logger = logger;

                try
                {
                    Uri host = await service.NextNewDomian();
                    if (host != null)
                        await exploreNewDomains.Process(host, new HashSet<string>()); 
                    else
                        Console.WriteLine("Нет новых доменов для проверки");
                }
                catch (OperationCanceledException ex)
                {
                    Console.WriteLine("Операция отменена");
                }
                catch (ServerException ex)
                {
                    HandleServerException(ex);
                }
                catch (Exception ex)
                {
                    HandleOtherException(ex);
                }
            };


            // Обход страниц со списками стайтей для прииска новых
            System.Timers.Timer timerFindNewArticles = TimerFor("FindNewArticles");
            timerFindNewArticles.Elapsed += async (o, e) =>
            {
                var findNewArticles = new FindNewArticles(new RemoteCoreService(), crawlerParametrs.UserAgent, ConsoleColor.Green);
                findNewArticles.CancellationToken = _cancel.Token;
                findNewArticles.Logger = logger;
                try
                {
                    await findNewArticles.Process();
                }
                catch (OperationCanceledException ex)
                {
                    Console.WriteLine("Операция отменена");
                }
                catch (ServerException ex)
                {
                    HandleServerException(ex);
                }
                catch (Exception ex)
                {
                    HandleOtherException(ex);
                }
            };

            // Другие задачи
        }

        private void HandleServerException(ServerException ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;

            Console.WriteLine("**************************************************************************");
            Console.WriteLine("Все задачи отменены, по причине ошибки на стороне сервера или связи с ним.");
            Console.WriteLine("**************************************************************************");
            Console.WriteLine("Причина: " + ex.GetaAllMessages());
            Console.ResetColor();
            logger.Error(ex, "Ошибка на стороне сервера или связи с ним");
            _cancel.Cancel();
 
        }

        private void HandleOtherException(Exception ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;

            Console.WriteLine("************************** Исключение ********************************");
            Console.WriteLine(ex.GetaAllMessages() + "\n" + ex.TargetSite);
            Console.ResetColor();
            logger.Error(ex, ex.GetaAllMessages());
        }

        private void HandleOperationCanceledException(OperationCanceledException ex)
        {
            Console.ForegroundColor = ConsoleColor.Red;

            Console.WriteLine("**************************************************************************");
            Console.WriteLine("Все задача отменена по причине поступления сигнала отмены");
            Console.WriteLine("**************************************************************************");
            Console.ResetColor();
        }

        private System.Timers.Timer TimerFor(string className)
        {
            System.Timers.Timer timer = new System.Timers.Timer();

            timer.Enabled = Boolean.Parse(ConfigurationManager.AppSettings[className + "_Enable"]); 
            timer.AutoReset = Boolean.Parse(ConfigurationManager.AppSettings[className + "_AutoReset"]);
            timer.Interval = int.Parse(ConfigurationManager.AppSettings[className + "_Interval"]);

            return timer;
        }

        public CrawlerArgs CrawlerPramsFromConfig()
        {
            CrawlerArgs args = new CrawlerArgs();
            // TODO: Проверить наличие всех параметров, иначе Исключение

            args.MaxDepth = int.Parse(ConfigurationManager.AppSettings["maxDepthLevel"]);
            args.MaxPages = int.Parse(ConfigurationManager.AppSettings["maxPages"]);
            args.UserAgent = ConfigurationManager.AppSettings["userAgent"];
            args.RequestTimeout = int.Parse(ConfigurationManager.AppSettings["requestTimeout"]);
            args.MinRequestsDelayMs = int.Parse(ConfigurationManager.AppSettings["minRequestDelay"]);
            args.CancellationToken = CancellationToken.None;

            return args;
        }

        public void Stop()
        {
            _cancel.Cancel();
        }

        public void ReadCommands()
        {
            PrintHelpMessage();
            do
            {
                switch (Console.ReadLine())
                {
                    case "start":
                        {
                            Start();
                            break;
                        }
                    case "stop":
                        {
                            Stop();
                            break;
                        }
                }
            } while (true);
        }

    }
}
