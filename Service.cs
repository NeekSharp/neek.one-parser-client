﻿using CommonLibrary;
using CommonLibrary.Faults;
using CommonLibrary.Helpers;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ParserClient
{
    public interface IRemoteCoreServiceProvider
    {
        Task<Uri> NextNewDomian();
        Task<Uri> NextExistingDomain();
        Task<HashSet<string>> KnownPagesForDomain(string host);
        Task SaveFeedPage(WebPage page, string text);
        Task UpdateKnownPagesList(string host, HashSet<string> pages);
        Task<Feed[]> FeedListForUpdate();
        Task SaveFeedsWithArticles(Feed[] feeds);
    }

 

    //TODO: Добавить обработку FaultExceptions
    public class RemoteCoreService : IRemoteCoreServiceProvider
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();


        public async Task SaveFeedsWithArticles(Feed[] feeds)
        {
            try {
                using (CoreServiceReference.ServiceClient service = new CoreServiceReference.ServiceClient())
                {
                    await service.SaveFeedsAsync(feeds);
                    PrintInfoMessage("Список статей отправлен на сервер.");
                }
            }
            catch (Exception ex)
            {
                ExecptionHendling(ex);
            }
        }

        public async Task UpdateKnownPagesList(string host,HashSet<string> pages)
        {
            string arrayInJson = Utils.JsonConverter.KnownPagesToJson(pages);

            try {
                using (CoreServiceReference.ServiceClient service = new CoreServiceReference.ServiceClient())
                {
                    await service.SaveKnownDomainsPagesAsync(host, arrayInJson);
                    PrintInfoMessage("Обновлены данные о страницах сайта на сервере.");
                }
            }
            catch (Exception ex)
            {
                ExecptionHendling(ex);
            }

        }

        public async Task<Uri> NextNewDomian()
        {
            string host = null;
            try
            {
                using (CoreServiceReference.ServiceClient service = new CoreServiceReference.ServiceClient())
                {
                    host = await service.NewDomainForCrawlingAsync();
                }
            }
            catch (Exception ex)
            {
                ExecptionHendling(ex);
            }
            PrintInfoMessage("Запрошен новый домен для проверки.");
            return CommonLibrary.Utils.TryCreateUri(host);
        }

        public async Task<Uri> NextExistingDomain()
        {
            try
            {
                string host;
                using (CoreServiceReference.ServiceClient client = new CoreServiceReference.ServiceClient())
                {
                    host = await client.UpdateFeedListForExistingDomainAsync();
                }
                PrintInfoMessage("Запрошен известный домен для проверки.");
                return CommonLibrary.Utils.TryCreateUri(host);
            }
            catch (Exception ex)
            {
                ExecptionHendling(ex);
            }
            return null;
        }

        public async Task<HashSet<string>> KnownPagesForDomain(string host)
        {
            HashSet<string> result = new HashSet<string>();
            string json = null;
            try
            {
                using (CoreServiceReference.ServiceClient client = new CoreServiceReference.ServiceClient())
                {
                    json = await client.KnownDomainPagesAsync(host);
                    PrintInfoMessage("Запрошены известные страницы сайта для проверки.");
                }
            }
            catch (Exception ex)
            {
                ExecptionHendling(ex);
            }

            if (!String.IsNullOrWhiteSpace(json))
            {
                result = Utils.JsonConverter.KnownPagesToHashSet(json);
            }

            return result;
        }

        public async Task<Feed[]> FeedListForUpdate()
        {
            Feed[] feeds = new Feed[0];
            try {
                using (CoreServiceReference.ServiceClient client = new CoreServiceReference.ServiceClient())
                {
                    feeds = await client.FeedsForUpdateAsync();
                    PrintInfoMessage("Запрошены страницы-источники для проверки.");
                }
            }catch(Exception ex)
            {
                ExecptionHendling(ex);
            }
            return feeds;
        }

        public async Task SaveFeedPage(WebPage page, string text)
        {
            try
            {
                using (CoreServiceReference.ServiceClient service = new CoreServiceReference.ServiceClient())
                {
                    await service.SaveFeedAsync(page.ToFeed(), text);
                    PrintInfoMessage("Сохранена странца-источник на сервер.");
                }
            }
            catch (Exception ex)
            {
                ExecptionHendling(ex);
            }
            
        }

        private void ExecptionHendling(Exception ex)
        {
            PrintExceptionMessage(String.Format("Возникло исключения при связи ссервером.{0}",ex.GetaAllMessages()));
            if (ex is CommunicationObjectFaultedException)
            {
                PrintExceptionMessage("Нет связи с сервером");
                logger.Fatal(ex,"Нет связи с сервером: " + ex.GetaAllMessages());
                throw new ServerException("Сервер недоступен.", ex);
            }
            else if (ex is FaultException)
            {
                if(ex is FaultException<DatabaseQueryFault>)
                {
                    string message = String.Format("Ошибка запроса к базе: {0}", ex.GetaAllMessages());
                    PrintExceptionMessage(message);
                    logger.Error(ex, message);
                }
                else if (ex is FaultException<DatabaseAccessFault>)
                {
                    string message = String.Format("Сервер не может получить доступа к без данных: {0}", ex.GetaAllMessages());
                    PrintExceptionMessage(message);
                    logger.Error(ex, message);
                    throw new ServerException("Сервер не смог получить доступ к базе", ex);
                }
                else
                {
                    string message = String.Format("Ошибка при обработки запроса сервером: {0}", ex.GetaAllMessages());
                    PrintExceptionMessage(message);
                    logger.Error(ex, message);
                }

                PrintExceptionMessage(String.Format("Не удалось получить домен для проверки: {0}", ex.GetaAllMessages()));
            }
            else
            {
                PrintExceptionMessage( String.Format("Не удалось получить домен для проверки: {0}", ex.GetaAllMessages()));
            }

        }

        public void PrintExceptionMessage(string msg)
        {
            Console.ForegroundColor = ConsoleColor.Black;
            Console.BackgroundColor = ConsoleColor.Red;
            Console.WriteLine("{0}\t{1}",DateTime.Now, msg);
            Console.ResetColor();
        }

        public void PrintInfoMessage(string msg)
        {
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.WriteLine("{0}\t{1}", DateTime.Now, msg);
            Console.ResetColor();
        }

    }
}
