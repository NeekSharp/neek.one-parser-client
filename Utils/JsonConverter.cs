﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserClient.Utils
{
    static class JsonConverter
    {
        public static HashSet<string> KnownPagesToHashSet(string json)
        {
            if (String.IsNullOrWhiteSpace(json))
            {
                return new HashSet<string>();
            }

            JObject parsed = JObject.Parse(json);
            IEnumerable<string> result = parsed["data"].ToArray().Select(a => a.ToString());
            return new HashSet<string>(result);
        }

        public static string KnownPagesToJson(HashSet<string> pages)
        {
            string array = JsonConvert.SerializeObject(pages);
            string result = "{\"data\":" + array + "}";

            return result;
        }
    }

}
