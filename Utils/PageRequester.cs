﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ParserClient.Crawler;
using System.Threading;
using System.Net;
using HtmlAgilityPack;
using CommonLibrary;
using System.IO;

namespace ParserClient.Utils
{
    public class PageRequester : IPageRequester
    {
        readonly int _timeout = 0;
        readonly string _userAgent = null;

        public PageRequester(string userAgent, int timeout = 3000)
        {
            _userAgent = userAgent;
            _timeout = timeout;
        }

        public WebPage DowloadPage(Link uri)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
            request.UserAgent = _userAgent;
            request.Accept = "*/*";
            request.Timeout = _timeout;
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            HttpWebResponse response = null;
            WebPage page = new WebPage(uri);

            try
            {
                using (response = request.GetResponse() as HttpWebResponse)
                {
                    string charset = "utf-8";
                    string ctype = response.Headers["content-type"];
                    if (ctype != null)
                    {
                        int ind = ctype.IndexOf("charset=");
                        if (ind != -1)
                        {
                            charset = ctype.Substring(ind + 8);
                        }
                    }

                    Encoding encoding = Encoding.GetEncoding(charset);

                    using (StreamReader str = new StreamReader(response.GetResponseStream()))
                    {
                        page.HttpStatus = response.StatusCode;

                        Encoding utf8 = Encoding.UTF8;
                        string html = str.ReadToEnd();
                        if (encoding != utf8)
                        {
                            byte[] anyBytes = encoding.GetBytes(html);
                            byte[] utf8Bytes = Encoding.Convert(encoding, utf8, anyBytes);

                            html = utf8.GetString(utf8Bytes);
                        }

                        page.Html = html;
                    }
                }
            }
            catch (WebException ex)
            {
                page.Exception = ex;
                using (WebResponse exresponse = ex.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)exresponse;
                    page.HttpStatus = httpResponse.StatusCode;
                }
            }
            catch (Exception ex)
            {
                page.Exception = ex;
                page.HttpStatus = HttpStatusCode.NotFound;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }

            return page;
        }

        public async Task<WebPage> DowloadPageAsync(Uri uri)
        {
            return await DowloadPageAsync(new Link(uri.AbsoluteUri));
        }

        public async Task<WebPage> DowloadPageAsync(Link uri)
        {
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(uri);
            request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
            request.UserAgent = _userAgent;
            request.Accept = "*/*";
            request.Timeout = _timeout;
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

            HttpWebResponse response = null;
            WebPage page = new WebPage(uri);

            try
            {
                using (response = await request.GetResponseAsync() as HttpWebResponse)
                {
                    string charset = "utf-8";
                    string ctype = response.Headers["content-type"];
                    if (ctype != null)
                    {
                        int ind = ctype.IndexOf("charset=");
                        if (ind != -1)
                        {
                            charset = ctype.Substring(ind + 8);
                        }
                    }

                    Encoding encoding = Encoding.GetEncoding(charset.Trim('"').ToLower());

                    using (StreamReader str = new StreamReader(response.GetResponseStream(), encoding))
                    {
                        page.HttpStatus = response.StatusCode;

                        Encoding utf8 = Encoding.UTF8;
                        string html = str.ReadToEnd();
                        if (encoding != utf8)
                        {
                            byte[] anyBytes = encoding.GetBytes(html);
                            byte[] utf8Bytes = Encoding.Convert(encoding, utf8, anyBytes);

                            html = utf8.GetString(utf8Bytes);
                        }


                        page.Html = html;
                    }
                }
            }
            catch (ProtocolViolationException ex)
            {
                page.Exception = ex;
            }
            catch (WebException ex)
            {
                page.Exception = ex;

                using (WebResponse exresponse = ex.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)exresponse;
                    page.HttpStatus = httpResponse.StatusCode;
                }
            }catch(Exception ex)
            {
                page.Exception = ex;
            }
            finally
            {
                if (response != null)
                {
                    response.Close();
                }
            }

            return page;
        }

    }
}
