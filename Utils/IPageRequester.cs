﻿using System;
using System.Threading.Tasks;
using ParserClient.Crawler;
using CommonLibrary;

namespace ParserClient.Utils
{
    public interface IPageRequester
    {
        WebPage DowloadPage(Link uri);
        Task<WebPage> DowloadPageAsync(Link uri);
    }
}
