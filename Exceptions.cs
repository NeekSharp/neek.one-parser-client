﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ParserClient
{
    [Serializable]
    class ServerException : Exception
    {
        public ServerException() { }

        public ServerException(string message) : base(message) { }

        public ServerException(string message, Exception inner) : base(message, inner) { }

        protected ServerException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
