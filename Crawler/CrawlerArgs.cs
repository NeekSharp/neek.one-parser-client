﻿using CommonLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ParserClient.Crawler
{
    public class CrawlerArgs
    {
        public CrawlerArgs()
        {
            CancellationToken = CancellationToken.None;
        }

        public CancellationToken CancellationToken { get; set; }

        /// <summary>
        /// UserAgent которым мы представляемся
        /// </summary>
        public string UserAgent { get; set; }

        /// <summary>
        /// Максимальная глубина просмотра относительно переданной страницы
        /// </summary>
        public int MaxDepth { get; set; }

        /// <summary>
        /// Это понятно что
        /// </summary>
        public int MaxPages { get; set; }

        /// <summary>
        /// Время ожидания ответе запрашиваемых страниц
        /// </summary>
        public int RequestTimeout { get; set; }

        /// <summary>
        /// Минимальная задержка между запросами к сайту при обходе, если оно не описано в robots.txt
        /// </summary>
        public int MinRequestsDelayMs { get; set; }
    }
}
