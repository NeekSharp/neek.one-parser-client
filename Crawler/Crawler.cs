﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using ParserClient.Utils;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using NLog;
using CommonLibrary.Helpers;
using CommonLibrary;
using System.Threading;

namespace ParserClient.Crawler
{
    class Crawler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// Default delay between requests to site, if it is not specified in robots txt
        /// </summary>
        private int _requestDelay = 200;

        /// <summary>
        /// When found new link on any page 
        /// </summary>
        public event EventHandler<LinksEventArgs> FoundLinksOnAPage = (e, a) => { };

        /// <summary>
        /// When new page has been loaded
        /// </summary>
        public event EventHandler<WebPageEventArgs> HaveNewPage = (e, a) => { };

        /// <summary>
        /// Ссылки по которым мы уже проходили
        /// </summary>
        public HashSet<Link> KnownPages { get; private set; }

        /// <summary>
        /// Ссылки на страницы которые не удалось получить
        /// </summary>
        public HashSet<Link> ErrorPages { get; set; }

        /// <summary>
        /// Представляет правила из файла robots.txt для сайта
        /// </summary>
        public RobotsTxt.Robots RobotsTxt { get; private set; }

        /// <summary>
        /// Ссылки с привденными словами не будут запрашиваться
        /// </summary>
        private static readonly Regex _ignoringWordsInLink = new Regex(@"redirect|forum|/http|=http|\.jpg|\.png|\.doc|\.mp3|\.gif|\.webp|\.xml|\.xls|\.ppt|\.rar|\.tar|\.gz|\.zip|\.torrent|\.7z|\.ttf|\.exe|\.bin|\.dll|\.rss|mailto:|#|\bpdf\b", RegexOptions.Compiled);

        CrawlerArgs _params;
        Link _primaryLink = null;

        public int DelayBetweenRequests {
            get { return _requestDelay; }
            set
            {
                if (value < _params.MinRequestsDelayMs) _requestDelay = _params.MinRequestsDelayMs;
                else _requestDelay = value;
            }
        }

        /// <summary>
        /// Инициализарует новый обходчик для сайта
        /// </summary>
        /// <param name="startPage"><see cref="CommonLibrary.Link"/>Ссылка на начальную страницу</param>
        /// <param name="args"><see cref="CrawlerArgs"/>Параметры обходчика</param>
        public Crawler(Link startPage, CrawlerArgs args)
        {
            _primaryLink = startPage;
            _params = args;
            KnownPages = new HashSet<Link>();
            ErrorPages = new HashSet<Link>();
        }

        /// <summary>
        /// Запуск обходчика сайта
        /// </summary>
        /// <returns>True если индексирвоание было разрешено и завершилось без исключений, false - всё остальное</returns>
        public async Task<bool> Run()
        {
            RobotsTxt = await GetRobotsTxt();
            DelayBetweenRequests = (int)RobotsTxt.CrawlDelay(_params.UserAgent);

            if (RobotsTxt.IsPathAllowed(_params.UserAgent, _primaryLink.PathAndQuery))
            {
                try {
                    await Crawl(_primaryLink, 0);
                }catch(OperationCanceledException)
                {
                    Console.WriteLine("Обход сайта {0} остановлен из-за отмены операции.", _primaryLink.AbsoluteUri);
                    throw;
                }catch(Exception ex)
                {
                    logger.Error(ex, "Обход сайта {0} завершился с исключением {1}", _primaryLink.AbsoluteUri, ex.GetaAllMessages());
                    Console.WriteLine("Обход сайта {0} завершился с исключением. Подробности в Error логе", _primaryLink.AbsoluteUri);
                    return false;
                }
            }
            else
            {
                string message = String.Format("Индексирование сайта {0} запрещено в UserAgent для \"{1}\"", _primaryLink.AbsoluteUri, _params.UserAgent);
                logger.Info(message);
                Console.WriteLine(message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Обходим все внутренние страницы раcположенные не глубже чем MaxDepth или пока не получим число страниц больше или равное MaxPages
        /// </summary>
        /// <returns></returns>
        /// <exception cref="OperationCanceledException">Инициируется если достигнуто максимальное количество страниц или поступил сигнал отмены</exception>
        private async Task Crawl(Link link,int currentDepthLevel)
        {
            KnownPages.Add(link);

            if (_params.CancellationToken.IsCancellationRequested) throw new OperationCanceledException(_params.CancellationToken);
            //if (currentDepthLevel > _params.MaxDepth) return;
            if (_params.MaxPages <= KnownPages.Count) return;

            await Task.Delay(DelayBetweenRequests);
            PageRequester requester = new PageRequester(_params.UserAgent,_params.RequestTimeout);
            WebPage page = await requester.DowloadPageAsync(link);

            if(page.Exception == null && page.HttpStatus == HttpStatusCode.OK)
            {
                page.InternalLinks = InternalLinksList(page);

                if (page.InternalLinks.Count > 0)
                {
                    HaveNewPage(this, new WebPageEventArgs(page)); // event

                    foreach (Link newLink in page.InternalLinks)
                    {
                        //if (_params.MaxPages <= KnownPages.Count) break;
                        
                        if (!KnownPages.Contains(newLink)
                            && !String.IsNullOrWhiteSpace(newLink.Title)
                            && !ErrorPages.Contains(newLink)
                            && RobotsTxt.IsPathAllowed(_params.UserAgent,newLink.PathAndQuery))
                        {
                            
                            await Crawl(newLink, currentDepthLevel + 1);                            
                        }
                    }
                }
            }
            else
            {
                ErrorPages.Add(page.Link);

                if (page.Exception != null)
                {
                    logger.Info(page.Exception, "Не удалось загрузить страницу {0}, по причине {1}", page.Link.AbsoluteUri, page.Exception.GetaAllMessages());
                }
                else
                {
                    logger.Info("Не удалось загрузить страницу {0}, код ответа {1}", page.Link.AbsoluteUri, page.HttpStatus);
                }
            }
                     
        }


        /// <summary>
        /// Ищем все внутренние ссылки на новые страницы, исключаются ссылки с якорями, ссылки н главную и на файлы
        /// </summary>
        /// <param name="page"></param>
        /// <returns>Коллекция Uri с текстом ссылки</returns>
        public List<Link> InternalLinksList(WebPage page)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.OptionReadEncoding = false;
            doc.OptionFixNestedTags = true;
            doc.OptionAutoCloseOnEnd = true;

            try { 
                doc.LoadHtml(page.Html);
            }catch(Exception ex)
            {
                logger.Error(ex,"Не удалось создать HtmlDocument страницы {0}", page.Link.AbsoluteUri);
            }

            HtmlNodeCollection ahrefs = doc.DocumentNode.SelectNodes(@"//a[@href]");
            
            List<Link> internalLinks = new List<Link>();
            if (ahrefs == null || ahrefs.Count == 0) return internalLinks;

            string host = _primaryLink.Host;
            string wwwhost = "www." + _primaryLink.Host;

            foreach (HtmlNode link in ahrefs)
            {
                string href = link.GetAttributeValue("href", "#").Trim();

                if (href.Equals("/") || _ignoringWordsInLink.IsMatch(href) || String.IsNullOrWhiteSpace(link.InnerText))
                {
                    continue;
                }
                else
                {
                    if (Uri.IsWellFormedUriString(href, UriKind.RelativeOrAbsolute))
                    {
                        Uri value = null;
                        if (Uri.TryCreate(href, UriKind.RelativeOrAbsolute, out value))
                        {
                            if (value.IsAbsoluteUri )
                            {
                                if (   value.Host.Equals(host,StringComparison.InvariantCultureIgnoreCase) 
                                    || value.Host.Equals(wwwhost, StringComparison.InvariantCultureIgnoreCase))
                                { 
                                    internalLinks.Add(new Link(value.AbsoluteUri, UriKind.Absolute, link.InnerText.Trim()));
                                }
                                else { 
                                    continue;
                                }
                            }
                            else
                            {
                                internalLinks.Add(new Link(new Uri(_primaryLink.Scheme + "://" + _primaryLink.Host), value, link.InnerText.Trim()));
                            }
                        }
                    }
                }
            }

            return internalLinks;
        }


        private async Task<RobotsTxt.Robots> GetRobotsTxt()
        {
            PageRequester req = new PageRequester(_params.UserAgent);

            WebPage page = await req.DowloadPageAsync(new Link("http://"+ _primaryLink.Host + "/robots.txt"));
            
            if(page == null || page.Html == null || page.Exception != null || page.HttpStatus != HttpStatusCode.OK)
            {
                return global::RobotsTxt.Robots.Load("");
            }
            else { 
                return global::RobotsTxt.Robots.Load(page.Html);
            }
        }
    }

    public class LinksEventArgs : EventArgs
    {
        public LinksEventArgs(Link source, List<Link> targets)
        {
            Source = source;
            Targets = targets;
        }

        public Link Source { get; set; }
        public List<Link> Targets { get; set; } 
    }

    public class WebPageEventArgs : EventArgs
    {
        public WebPageEventArgs(WebPage page)
        {
            Page = page;
        }
        public WebPage Page { get; set; }
    }

}
