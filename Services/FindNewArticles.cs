﻿using System;
using System.Threading.Tasks;
using CommonLibrary;
using HtmlToFeed;
using System.Collections.Generic;
using ParserClient.Utils;
using System.Drawing;
using System.Diagnostics;
using System.Linq;

namespace ParserClient.Services
{
    class FindNewArticles : BaseService
    {
        private readonly string _userAgent = null;
        IRemoteCoreServiceProvider _service = null;

        public FindNewArticles(IRemoteCoreServiceProvider service, string useragent, ConsoleColor textcolor)
        {
            _userAgent = useragent;
            _service = service;
            color = textcolor;
        }

        /// <summary>
        /// Получаем очереднуую порцию страницы со списками статей и парсим их
        /// </summary>
        /// <returns></returns>
        public async Task Process()
        {
            // Получаем список страниц для проверки новых ссылок
            Feed[] feeds = await _service.FeedListForUpdate();

            foreach (Feed feed in feeds)
            {
                _cancellationToken.ThrowIfCancellationRequested();

                // Получаем каждую страницу
                WebPage page = await RequestPage(feed.Uri);
                if (page.Exception != null || page.Html == null)
                {
                    // Добавляем инфрмацию о неудачной попытке ее получить
                    PrintMessage(String.Format("Проверка {0}, завершилась ошибкой.", feed.Uri), color);
                    AppendFailureInfo(feed, page);
                    continue;
                }

                PrintMessage(String.Format("Проверика списка статей страницы {0}", page.Link.AbsoluteUri),color);
                // Парсим страницу на предмет списка статей
                Html2Feed h2f = new Html2Feed(feed.Uri, page.Html);

                // Получаем список найденных статей
                List<FeedItem> posts = h2f.ListElements();
                
                //posts.Where(a => feed.Uri.IsBaseOf(a.Link));
                Debug.Assert(posts != null, "Список статей со страницы равен null");


                if (posts.Count > 0)
                {
                    PrintMessage(String.Format("Найдены статьи {0}", feed.Uri), color);
                    // Получаем для каждой статьи изображения 
                    string host = feed.Uri.Host.Replace("/www.", "/");
                    posts = posts.Where(a => a.Link.Host.Contains(host)).ToList();
                    await GetPostsImagesUrls(posts);

                    // Добавляем статьи к источнику
                    feed.Contents = ContentListFromPage(posts);
                }
                else
                {
                    PrintMessage(String.Format("Не найдены почты {0}", feed.Uri), color);
                }
            }

            // Отправляем обновленные источники на обратно сервис 
            if (feeds.Length > 0)
            {
                await _service.SaveFeedsWithArticles(feeds);
                PrintMessage(String.Format("Получен и сохранен контент страниц-источников сайта {0}", feeds[0].Uri.Host), ConsoleColor.Black, color);
            }

        }

        /// <summary>
        /// Переводим Content к виду пригодному для передачи
        /// </summary>
        /// <param name="posts"></param>
        /// <returns></returns>
        private ContentItem[] ContentListFromPage(List<FeedItem> posts)
        {
            ContentItem[] contentData = new ContentItem[posts.Count];
            for (int i = 0; i < posts.Count; i++)
            {
                contentData[i] = posts[i].Serialized();
            }
            return contentData;
        }

        /// <summary>
        /// Поиск самый больших по изображений для каждой статьи исходя из их разрешения
        /// </summary>
        /// <param name="posts"></param>
        /// <returns></returns>
        private async Task GetPostsImages(List<FeedItem> posts)
        {
            for (int i = 0; i < posts.Count; i++)
            {
                Image img = null;
                try { 
                    img = await Images.ImageProcessor.FindImage(posts[i], posts[i].Html, 200, 200);
                }
                catch (Exception ex)
                {
                    _logger.Info(ex,"Не удалось получиь изображение со для поста {0}", posts[i].Link);
                }

                if (img != null)
                {
                    //posts[i].Image = Images.ImageProcessor.ImageToByteArray(Images.ImageProcessor.ResizeImage(img, 150, 150));
                }
            }

        }

        /// <summary>
        /// Поиск изображений для статьи исходя их их размера в заголовке http запроса.
        /// </summary>
        /// <param name="posts"></param>
        /// <returns></returns>
        private async Task GetPostsImagesUrls(List<FeedItem> posts)
        {
            for (int i = 0; i < posts.Count; i++)
            {
                Uri uri = null;
                try
                {
                    uri = await Images.ImageProcessor.FindImageByHeaders(posts[i], posts[i].Html, 200, 200);
                }
                catch (Exception ex)
                {
                    _logger.Info(ex, "Не удалось получиь изображение со для поста {0}", posts[i].Link);
                }

                if (uri != null)
                {
                    posts[i].ImageUri = uri;
                }
            }
        }

        private async Task<WebPage> RequestPage(Uri uri)
        {
            PageRequester requester = new PageRequester(_userAgent);
            return await requester.DowloadPageAsync(uri);
        }

        private void AppendFailureInfo(Feed feed, WebPage page)
        {
            if(page.Exception != null) {
                feed.Comment = page.Exception.Message;
                feed.Fail = true;
            }
            else if(page.Html == null)
            {
                feed.Comment = "Html не был получен";
                feed.Fail = true;
            }

        }

    }
}
