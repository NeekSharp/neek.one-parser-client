﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using NLog;
using CommonLibrary.Helpers;

namespace ParserClient.Services
{
    public abstract class BaseService
    {
        protected CancellationToken _cancellationToken;
        protected Logger _logger = null;
        protected bool _verbose = false;
        protected string _serviceName;
        public ConsoleColor color = ConsoleColor.White;

        public CancellationToken CancellationToken
        {
            get
            {
                return _cancellationToken;
            }

            set
            {
                _cancellationToken = value;
            }
        }

        // TODO: Может быть не задан, но испольуется без проверки, беда
        public Logger Logger {
            set
            {
                _logger = value;
            }
        }

        public void PrintMessage(string msg, ConsoleColor foreground, ConsoleColor background = ConsoleColor.Black)
        {

            Console.ForegroundColor = foreground;
            Console.BackgroundColor = background;

            Console.WriteLine("{0}\t{1}", DateTime.Now, msg);

            Console.ResetColor();
        }

        //abstract public Task Process(Uri domain, HashSet<string> knownPages);

        protected string ServiceName
        {
            get {
                if (_serviceName == null ) _serviceName = this.GetType().Name;
                return _serviceName;
            }
        }

        public void RunTask(Task task, CancellationTokenSource cancellationTokenSource)
        {
            try {
                task.Start();
                task.Wait();
            }catch(AggregateException ex)
            {
                if(ex.InnerExceptions.Any(a=>a is ServerException))
                {
                    PrintMessage("**************************************************************************", ConsoleColor.Black, ConsoleColor.Red);
                    PrintMessage("Все задачи отменены, по причине ошибки на стороне сервера или связи с ним.", ConsoleColor.Black, ConsoleColor.Red);
                    PrintMessage("**************************************************************************", ConsoleColor.Black, ConsoleColor.Red);
                    cancellationTokenSource.Cancel();
                }

                PrintMessage("*************************** Внутренние исключения ************************", ConsoleColor.Black, ConsoleColor.Red);
                PrintMessage(String.Format("*************************** В задаче {0} ************************", ServiceName), ConsoleColor.Black, ConsoleColor.Red);
                foreach (Exception subex in ex.InnerExceptions)
                {
                    if(_logger != null) { 
                        _logger.Error(subex, ex.Message);
                    }
                    PrintMessage(ex.Message, ConsoleColor.Black, ConsoleColor.Red);
                }
            }

        }
    }
}
