﻿using CommonLibrary;
using CommonLibrary.Helpers;
using Newtonsoft.Json;
using ParserClient.Crawler;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ArticlesListDetector;

namespace ParserClient.Services
{
    class ExploreWebSite : BaseService
    {
        private HashSet<string> _knownPages = null;
        private IRemoteCoreServiceProvider _service = null;
        private CrawlerArgs _crawlerArgs = null;

        /// <summary>
        /// Создает экземпляр сервиса для обхода сайтов
        /// </summary>
        /// <param name="service">Внешний сервис для получения и сохранения данных</param>
        /// <param name="crawlerArgs">Пареметры для обходчика сайта <see cref="CrawlerArgs"/></param>
        /// <param name="messageColor">Уникальный цвет консольного вывода для сообщений от этого сервиса</param>
        public ExploreWebSite(IRemoteCoreServiceProvider service, CrawlerArgs crawlerArgs, ConsoleColor messageColor) {

            _service = service;
            _crawlerArgs = crawlerArgs;
            
            color = messageColor;
        }

        /// <summary>
        /// Формирование задачи на обход сайта
        /// </summary>
        /// <param name="domain">Начальная страница для обхода</param>
        /// <param name="knownPages">Известные страницы сайта от предыдущих обходов, будут игнорироваться</param>
        /// <returns></returns>
        public async Task Process(Uri domain, HashSet<string> knownPages)
        {
            Uri host = domain;
            _knownPages = knownPages;

            _crawlerArgs.CancellationToken = CancellationToken;
            Crawler.Crawler crawler = new Crawler.Crawler(new Link(host.AbsoluteUri), _crawlerArgs);
            crawler.HaveNewPage += HandleNewPage;

            try
            {
                PrintMessage(String.Format("Начат обход сайта {0}", host), color);
                await crawler.Run();
                PrintMessage(String.Format("Завершен обход сайта {0}.", host), color);
            }
            catch (OperationCanceledException)
            {
                PrintMessage(String.Format("Обход сайта {0} завершен из-за отмены операции.", host), color);
                throw;
            }
            catch (Exception ex)
            {
                string message = String.Format("Обход сайта {0} завершен. Возникло непредвиденное исключение {1}.", host, ex.GetaAllMessages());
                _logger.Info(ex, message);
                PrintMessage(message, color);
            }
            finally
            {
                if (_knownPages.Count > 0)
                {
                    PrintMessage(String.Format("Отправка списка исследованных страниц {0} на сервер.", host), color);
                    await _service.UpdateKnownPagesList(host.Host, _knownPages);
                }
            }
        }

        private async void HandleNewPage(object sender, WebPageEventArgs args)
        {
            if (_knownPages.Contains(args.Page.Link.PathAndQuery) || String.IsNullOrWhiteSpace(args.Page.Link.Title)) return;
            
            if (args.Page != null
                && args.Page.Exception == null
                && args.Page.HttpStatus == System.Net.HttpStatusCode.OK
                && args.Page.Html != null)
            {
                ListDetector listDetector = new ListDetector(args.Page);

                bool isList = listDetector.IsList();

                _knownPages.Add(args.Page.Link.PathAndQuery);
                if (isList)
                {
                    PrintMessage(String.Format("Страница списка статей\t\t{0}", args.Page.Link.AbsoluteUri), color);
                    await _service.SaveFeedPage(args.Page, listDetector.InnerText());
                }
                else
                {
                    PrintMessage(String.Format("Не найдено списка статей\t\t{0}", args.Page.Link.AbsoluteUri), color);
                    args.Page.Html = null;
                }
            }
            else
            {
                PrintMessage(String.Format("Попытка получить страницу {0} завершилась неудачно.{1}", args.Page.Link.AbsoluteUri, args.Page.Exception.GetaAllMessages()), color);
            }
        }

    }
}
