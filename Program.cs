﻿using ParserClient.Services;
using System.Collections.Generic;
using System.Runtime.CompilerServices;


[assembly: InternalsVisibleTo("ParserClient.Test")]

namespace ParserClient
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceManager sm = new ServiceManager();
            sm.ReadCommands();
        }
    }
}
